<?php
/**
 * @file bt_export_ds.module
 * Contains function for export display suite data
 */



/**
 *  return a list of view modes used in the export form
 *
 */
function bt_export_ds_list_view_modes(){
	$entity_view_modes = array();
	$view_modes = array();
	$entity_info = entity_get_info('node');
	if(!empty($entity_info['view modes'])){
		$entity_view_modes = array_keys($entity_info['view modes']);
		foreach($entity_view_modes as $delta => $name){
			$view_modes[$name] = $name;
		}
	}
	return $view_modes;
}


/**
 * @param $bundle
 *   content type bundle
 * @param $field_groups
 *   bool
 *
 * This function will return Display Suite fields groups or view modes
 */
function bt_export_ds_view_modes_export($bundle, $entity_type = 'node', $field_groups = FALSE, $extract_view_mode_keys = FALSE, StdCLass &$export = NULL){
	$ds_view_modes = new StdClass();
	//get the view modes
	$view_modes = ctools_export_crud_load_all('ds_view_modes');
	//add in the "full" view mode
	$view_modes['full'] = (object) array(
		'view_mode' => 'full',
		'label' => 'Full',
	);
	$view_modes['search_result'] = (object) array(
		'view_mode' => 'search_result',
		'label' => 'Search Results',
	);
	$view_modes['default'] = (object) array(
		'view_mode' => 'default',
		'label' => 'Default',
	);

	//loop through the view modes and load view mode instances
	foreach($view_modes as $values){
		$ds_view_mode = ds_get_layout($entity_type, $bundle, $values->view_mode);
		if(!empty($ds_view_mode)){
			switch($field_groups){
			case TRUE:
				$ds_view_modes->{$values->view_mode} = field_group_info_groups($entity_type, $bundle, $values->view_mode);
				break;
			default:
				//add the view mode instances to our return array
				$ds_view_modes->{$values->view_mode} = new StdClass();
				//set the display suit layout settings
				$ds_view_modes->{$values->view_mode}->ds_layout_settings = $ds_view_mode;
				//set the view mode label
				if($values->view_mode != 'full'){
					$ds_view_modes->{$values->view_mode}->view_mode_properties = $view_modes[$values->view_mode];
				}
				//get the display suit field settings
				$ds_field_settings = ds_get_field_settings($entity_type, $bundle, $values->view_mode);
				if(!empty($ds_field_settings)){
					$ds_view_modes->{$values->view_mode}->ds_field_settings = $ds_field_settings;
				}
				if($extract_view_mode_keys && !empty($ds_view_mode)){
					$export->custom_fields['view_modes'][$values->view_mode] = $values->view_mode;
				}
				break;
			}
		}
	}
	return $ds_view_modes;
}




/**
 * @param $settings
 *   array of view modes with bundles as children
 * @return $export
 *   our export object
 */
function bt_export_ds_extract_multiple_bundle_custom_fields($settings = array(), &$export){
	$all_custom_fields = array();
	$custom_field_per_bundle = array();
	foreach($settings as $view_mode => $bundles){
		$custom_field_per_bundle = bt_export_ds_extract_custom_fields($bundles, array($view_mode));
		if(!empty($all_custom_fields)){
			$all_custom_fields = array_merge($all_custom_fields, $custom_field_per_bundle);
		}else{
			$all_custom_fields = $custom_field_per_bundle;
		}
	}
	if(!empty($all_custom_fields)){
		$export->components['custom_fields'] = TRUE;
		$export->custom_fields = array();
		$export->custom_fields = $all_custom_fields;
	}
}




/**
 * @param $bundles
 *   an array of bundles
 * @return $view_modes
 *   an array of view modes
 */
function bt_export_ds_extract_custom_fields($bundles = array(), $view_modes = array()){
	$custom_fields = array();
	$fields = ds_get_fields('node');
	$all_custom_fields = ctools_export_crud_load_all('ds_fields');
	$ignore_fields = array('search_snippet', 'comments', 'search_info');
	foreach($bundles as $delta => $bundle){
		foreach ($fields as $key => $field){
			foreach($view_modes as $name => $view_mode){
				// Check on ui_limit.
				if (isset($field['ui_limit'])) {
					$continue = FALSE;
					foreach ($field['ui_limit'] as $limitation) {
						list($limit_bundle, $limit_view_mode) = explode('|', $limitation);
						if ($limit_bundle == '*' && !in_array($key, $ignore_fields) || $limit_bundle == $bundle && !in_array($key, $ignore_fields)) {
							//$continue = TRUE;
							if ($limit_view_mode == '*' || $limit_view_mode == $view_mode) {
								$continue = TRUE;
							}
						}
					}
					if ($continue) {
						$field['entities'] = $all_custom_fields[$key]->entities;
						$custom_fields[$key] = $field;
					}
				}
			}
		}
	}
	return $custom_fields;
}




/**
 * @param $modes
 *   an array of view modes
 * @param $export
 *   our export object
 * @param $custom_fields
 *   bool whether to extract custom display suite fields
 */
function _bt_export_node_view_modes($modes = array(), &$export, $custom_fields = FALSE, $field_collections = FALSE, $field_groups = FALSE){
	$view_modes = array();
	$entities = array();
	$ds_view_modes = new StdClass();
	$extract = FALSE;
	foreach($modes as $name => $value){
		if(!empty($value)){
			$view_modes[$name] = TRUE;
			$extract = TRUE;
		}
	}
	if($extract){
		$bundle_type = node_type_get_types();
		$bundle_type = array_keys($bundle_type);
		$entities['node'] = $bundle_type;
		if($field_collections){
			$field_collection_items = bt_export_list_field_collections();
			$entities['field_collection_item'] = array_keys($field_collection_items);
		}
		foreach($entities as $entity_type => $bundles){
			$view_mode_settings = new BtExportDsViewModes();
			$view_mode_settings->exportViewModes($view_modes, $bundles, $export, $custom_fields, $field_collections, $entity_type, $field_groups);
		}
	}else{
		unset($export->components['advanced_ds']);
	}
}



function bt_export_field_collection_view_modes($modes = array(), &$export, $custom_fields = FALSE, $field_collections = FALSE){

}