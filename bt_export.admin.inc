<?php

/**
 * @file bt_export.admin.inc
 * Export / Import form
 */


//our import / export form
function _bt_export_form($form, &$form_state){

	$form = array();
	drupal_add_library('system', 'drupal.collapse');
	module_load_include('class.php', 'bt_export', 'classes/bt_export_ds_view_modes');
	module_load_include('inc', 'bt_export', 'includes/bt_export_form_handler_functions');
	drupal_add_css(drupal_get_path('module', 'bt_export') . '/css/bt_export_form.css', array('region' => 'header'));

	$form['#prefix'] = '<div id="bt-export-form">';
	$form['#suffix'] = '</div>';

	//import section
	//import vertcle tab
	$form['wrapper'] = array(
		'#type' => 'vertical_tabs',
	);
	$form['imports_wrapper'] = array(
		'#type' => 'fieldset',
		'#title' => t('Import'),
		'#collapsed' => FALSE,
		'#collapsible' => TRUE,
		'#group' => 'wrapper',
		'#attributes' => array(
			'id' => 'bt-export-import-container',
		),
	);
	$form['imports_wrapper']['imports_fieldset'] = array(
		'#type' => 'fieldset',
		'#title' => t('Import Code'),
		'#collapsed' => FALSE,
		'#collapsible' => FALSE,
		'#attributes' => array(
			'id' => 'bt-export-import-fieldset',
		),
	);
	$form['imports_wrapper']['imports_fieldset']['import'] = array(
		'#title' => t('Code'),
		'#type' => 'text_format',
		'#cols' => 60,
		'#rows' => 20,
		'#default_value' => '',
		'#format' => 'plain_text',
		'#prefix' => '<div id="bt-import-code-field">',
		'#suffix' => '</div>',
	);
	$form['imports_wrapper']['imports_fieldset']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Import'),
		'#submit' => array('_bt_export_import_submit'),
		'#ajax' => array(
			'wrapper' => 'bt-export-import-fieldset',
			'callback' => '_bt_export_ajax_form_callback',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#prefix' => '<div id="bt-import-submit">',
		'#suffix' => '</div>',
	);

	$form['imports_wrapper']['imports_fieldset']['import_functions'] = array(
		'#type' => 'hidden',
	);

	//import ajax links
	if(!empty($form_state['import_components'])){
		//import ajax fields
		$form['imports_wrapper']['imports_fieldset']['import_functions'] = array(
			'#type' => 'fieldset',
			'#title' => t('Import Functions'),
			'#collapsed' => FALSE,
			'#collapsible' => TRUE,
			'#attributes' => array(
				'id' => 'bt-export-import-fieldset',
			),
		);
		//results chanellog
		$form['imports_wrapper']['imports_fieldset']['import_functions']['chanel_log'] = array(
			'#type' => 'fieldset',
			'#collapsible' => TRUE,
			'#collapsed' => empty($form_state['import_results']) ? TRUE : FALSE,
			'#title' => t('Results Log'),
			'#prefix' => '<div id="bt-import-chanel-log">',
			'#suffix' => '</div>',
		);
		$form['imports_wrapper']['imports_fieldset']['import_functions']['chanel_log']['prev_results'] = array(
			'#type' => 'markup',
			'#markup' => '',
			'#prefix' => '<div id="bt-import-chanelog-results" class="clearfix">',
			'#suffix' => '</div>',
		);
		//results
		if(!empty($form_state['import_results'])){
			$form['imports_wrapper']['imports_fieldset']['import_functions']['chanel_log']['results'] = array(
				'#type' => 'markup',
				'#markup' => render($form_state['import_results']),
				'#prefix' => '<div id="bt-import-chanelog-results" class="clearfix">',
				'#suffix' => '</div>',
			);
		}
		foreach($form_state['import_components'] as $component => $bundles){
			$form['imports_wrapper']['imports_fieldset']['import_functions'][$component] = array(
				'#type' => 'fieldset',
				'#title' => $component,
				'#collapsed' => FALSE,
				'#collapsible' => TRUE,
			);
			foreach($bundles as $name => $import_settings){
				$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name] = array(
					'#type' => 'fieldset',
					'#title' => $name,
					'#collapsed' => FALSE,
					'#collapsible' => TRUE,
				);
				switch($component){
				case 'advanced_ds':
					foreach($import_settings as $view_mode => $view_mode_bundles){

						if(!empty($view_mode_bundles['name'])){
							$field_name = $view_mode_bundles['name'];
							$title = $view_mode_bundles['title'];
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$field_name] = _bt_export_build_ajax_field($field_name, $title);
						} else {
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode] = array(
								'#type' => 'fieldset',
								'#title' => $view_mode,
								'#collapsed' => FALSE,
								'#collapsible' => TRUE,
							);
							foreach($view_mode_bundles as $type => $values){
								$field_name = $values['name'];
								$title = $values['title'];
								switch($type){
								case 'field_instances':

									$instance_amounts = _bt_export_find_import_ds_bundle_dta_counters($values, $form_state, $type);
									$num_of_imported_instances = $instance_amounts['imported'];
									$num_of_instances = $instance_amounts['all'];
									$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode]['container'] = _bt_export_field_instance_container();
									$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode]['container'][$title] = _bt_export_build_ajax_field($field_name, $title);
									$input_testfield_name = $values['input_name'];
									$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode]['container'][$input_testfield_name] = _bt_export_instance_amount_textfield($num_of_instances);
									$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode]['container']['instance_import_status'] =_bt_export_instance_amount_markup($num_of_instances, $num_of_imported_instances);

									break;
								default :
									$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$view_mode][$title] = _bt_export_build_ajax_field($field_name, $title);
									break;
								}
							}
						}
					}
					break;
				default:

					foreach($import_settings as $import_name => $settings){
						$field_name = $settings['name'];
						$title = $settings['title'];
						switch($settings['type']){
						case 'field_instances':
							$instance_amounts = _bt_export_find_import_bundle_data_counters($settings, $form_state, $settings['type']);
							$num_of_imported_instances = $instance_amounts['imported'];
							$num_of_instances = $instance_amounts['all'];
							$bundle = $settings['bundle'];
							/* dpm($field_name); */
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$import_name]['container'] = _bt_export_field_instance_container();
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$import_name]['container'][$title] = _bt_export_build_ajax_field($field_name, $title);
							$input_testfield_name = $settings['input_name'];
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$import_name]['container'][$input_testfield_name] = _bt_export_instance_amount_textfield($num_of_instances);
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$import_name]['container']['instance_import_status'] =_bt_export_instance_amount_markup($num_of_instances, $num_of_imported_instances);
							break;
						default:
							$form['imports_wrapper']['imports_fieldset']['import_functions'][$component][$name][$import_name] = _bt_export_build_ajax_field($field_name, $title);
							break;
						}

					}
					break;
				}

			}
		}
	}
	//end import section


	//export section

	//export verticle tab
	$form['export_wrapper'] = array(
		'#type' => 'fieldset',
		'#title' => t('Export'),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
		'#group' => 'wrapper',
	);
	//content types fieldset
	$form['export_wrapper']['content_types'] = array(
		'#type' => 'fieldset',
		'#title' => t('Content Types'),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
	);
	//taxonomy export
	$form['export_wrapper']['taxonomy_terms'] = array(
		'#type' => 'fieldset',
		'#title' => t('Taxonomy'),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
	);
	if(_bt_export_export_components('bt_export_taxonomy')){
		$taxonomy_vocabularies = bt_export_taxonomy_handler();
		$form['export_wrapper']['taxonomy_terms']['taxonomy_terms'] = array(
			'#title' => t('Select Taxonomy Vocabularies'),
			'#type' => 'checkboxes',
			'#options' => $taxonomy_vocabularies,
			'#default_value' => array(),
			'#multiple' => TRUE,
		);
	} else {
		$form['export_wrapper']['taxonomy_terms']['taxonomy_info'] = array(
			'#type' => 'markup',
			'#markup' => 'Enable the "Blue Tree Export Taxonomy Module" to access this feature.',
		);
	}


	//display suite view modes
	$form['export_wrapper']['ds_view_modes_fieldsets'] = array(
		'#type' => 'fieldset',
		'#title' => t('Advanced Display Suite'),
		'#collapsed' => TRUE,
		'#collapsible' => TRUE,
	);
	//check if bt_export_ds module is enabled
	if(_bt_export_export_components('bt_export_ds')){
		//advanced display suit
		//get all the entity view modes
		$view_modes = bt_export_ds_list_view_modes();
		//taxonomy export
		$form['export_wrapper']['ds_view_modes_fieldsets']['ds_view_modes'] = array(
			'#title' => t('View Mode'),
			'#type' => 'checkboxes',
			'#options' => $view_modes,
			'#multiple' => TRUE,
			'#description' => t('Export all Field Groups, Field Settings and Layout Settings for each node using the selected View Mode(s).'),
		);
		//checkbox states
		$states = array();
		foreach($view_modes as $name => $value){
			$states['visible'][] = array(':input[name="ds_view_modes['.$name.']"]' => array('checked' => TRUE));
		}
		$form['export_wrapper']['ds_view_modes_fieldsets']['advanced_ds_custom_fields'] = array(
			'#title' => t('Display Suite Custom Fields'),
			'#type' => 'checkboxes',
			'#options' => array(
				'yes' => 'yes',
			),
			'#description' => t('Export Code Fields for selected View Mode(s)'),
			'#states' => $states,
		);
		if(_bt_export_export_components('bt_export_field_collection')){
			//advanced ds field collections
			$form['export_wrapper']['ds_view_modes_fieldsets']['advanced_ds_field_collections'] = array(
				'#title' => t('Field Collection DS Settings'),
				'#type' => 'checkboxes',
				'#options' => array(
					'yes' => 'yes',
				),
				'#description' => t('Export Field Collection DS setting for selected View Mode(s)'),
				'#states' => $states,
			);
		}
		if(_bt_export_export_components('bt_export_field_group')){
			//advanced ds field groups
			$form['export_wrapper']['ds_view_modes_fieldsets']['advanced_ds_field_groups'] = array(
				'#title' => t('Field Groups'),
				'#type' => 'checkboxes',
				'#options' => array(
					'yes' => 'yes',
				),
				'#description' => t('Export DS Field Groups for selected View Mode(s)'),
				'#states' => $states,
			);
		}
	} else {
		$form['export_wrapper']['ds_view_modes_fieldsets']['ds_info'] = array(
			'#type' => 'markup',
			'#markup' => 'Enable the "Blue Tree Export Ds Module" to access this feature.',
		);
	}


	//get all the content types
	$content_types = node_type_get_types();
	$content_types = array_keys($content_types);
	$select_content_types = array(
		'_none' => '-Select Content Type-',
	);
	foreach($content_types as $delta => $value){
		$select_content_types[$value] = $value;
	}
	//select content type
	$form['export_wrapper']['content_types']['export_content_type'] = array(
		'#type' => 'select',
		'#title' => t('Select Content Type'),
		'#options' => $select_content_types,
		'#multiple' => TRUE,
		'#size' => 10,
	);
	//export compenent options depending on which module are available
	$export_components = _bt_export_export_components();
	//export component types
	$form['export_wrapper']['content_types']['export_types'] = array(
		'#title' => t('Export components'),
		'#type' => 'checkboxes',
		'#options' => $export_components,
		'#multiple' => TRUE,
	);
	if(!empty($export_components['ds'])){
		$form['export_wrapper']['content_types']['ds_custom_fields'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Display Suite Custom Fields'),
			'#options' => array(
				'yes' => t('Yes'),
			),
			'#states' => array(
				'visible' => array(   // action to take.
					':input[name="export_types[ds]"]' => array('checked' => TRUE),
				),
			),
		);
	}

	if(!empty($export_components['field_group']) && !empty($export_components['ds'])){
		//our field group options per module
		$field_group_options = array();
		if(!empty($export_components['field_group'])){
			$field_group_options['form_field_groups'] = t('Form Field Groups');
			if(!empty($export_components['ds'])){
				$field_group_options['ds_field_groups'] = t('Display Suite Field Groups');
			}
		}
		//if we have field group options
		//create our field group checkboxes
		if(!empty($field_group_options)){
			$form['export_wrapper']['content_types']['export_field_groups_type'] = array(
				'#title' => t('Field Group Types'),
				'#type' => 'checkboxes',
				'#options' => $field_group_options,
				'#multiple' => TRUE,
				'#states' => array(
					'visible' => array(   // action to take.
						':input[name="export_types[field_group]"]' => array('checked' => TRUE),
					),
				),
			);
		}
	}
	$form['export_wrapper']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Export'),
		'#submit' => array('_bt_export_submit'),
	);
	if(!empty($form_state['values'])){
		$form['export_wrapper']['code_placeholder'] = array(
			'#type' => 'text_format',
			'#cols' => 60,
			'#rows' => 20,
			'#title' => t('Export Code'),
			'#default_value' => !empty($form_state['code']) ? $form_state['code'] : NULL,
			'#prefix' => '<div id="bt-export-select">',
			'#suffix' => '</div>',
			'#format' => 'plain_text',
		);
	}

	//end export section
	//$form['#validate'] = array('_bt_export_validate');

	return $form;
}