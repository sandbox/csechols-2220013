<?php
/*



class BtExportFieldCollections extends BtImportContentType{
	
	public $field_instances;
	public $field_settings;
	
	public function __construct($field_settings, $field_instances){
		$this->chaneLog = new btExportChanelog();
		$this->field_settings = $field_settings;
		$this->field_instances = $field_instances;
	}
	
	
	public function importFieldCollections(array $field_settings, array $field_instances){
		$field_settings = !empty($field_settings) ? $field_settings : $this->field_settings;
		$field_instances = !empty($field_instances) ? $field_instances : $this->field_instances;
		foreach($field_settings as $bundle => $field_collection_field){
			foreach($field_collection_field as $field_collection_name => $field_collection_fields){
				foreach($field_collection_fields as $field_name => $field_values){
					$field_exists = field_info_field($field_name);
					$current_instance = field_info_instance('field_collection_item', $field_name, $field_collection_name);
					$field_instance = !empty($field_instances[$bundle][$field_collection_name][$field_name]) ? $field_instances[$bundle][$field_collection_name][$field_name] : array();
					//check to see if this field instance ahs changed
					$change = !empty($field_instance['display']) && $field_instance['display'] === $current_instance['display'] ? TRUE : FALSE;
					//if the field exists
					if($field_exists){
						if(empty($current_instance)){
							//create the field instance
							field_create_instance($field_instance);
							$this->chaneLog->chanelUpdateChanelog('chanelUpdateMessage', $bundle, 'field_instances', $field_name, 'created');
						}else if(!$change && !empty($field_instance)){
							//update the instance
							$current_instance['display']+$field_instance['display'];
							field_update_instance($current_instance);
							$this->chaneLog->chanelUpdateChanelog('chanelUpdateMessage', $bundle, 'field_instances', $field_name, 'updated');
						}
					} else {
						$create_field = field_create_field($field_values);
						//create the field instance if its available
						if(!empty($field_instance)){
							$create_instance = field_create_instance($field_instance);
						}
						$this->chaneLog->chanelUpdateChanelog('chanelUpdateMessage', $bundle, 'field_new', $field_name, 'created');
					}
				}
			}
		}
		return $this->fCleanUp();
	}
	
	
	protected function fCleanUp(){
		return $this->chaneLog->cleanUp();
	}
	
}
*/