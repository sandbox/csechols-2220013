<?php

/**
 * @file bt_export_ds_view_modes
 * Contains functions for export display suite settings
 */

//this class could be deleted and turned into functions
// it contained more function which are no longer needed
class BtExportDsViewModes {

	public $view_modes;
	public $content_types;
	public $field_groups = array();


	public function __construct(){
	}

	/**
	 * Extracts the Display Suite data (layout_settings, field_settings, field_groups, ect..) from different entity_types being exported
	 * 
   */
	public function exportViewModes($view_modes = array(), $bundles = array(), &$export, $extract_custom_fields = FALSE, $field_collections = FALSE, $entity_type = 'node', $field_groups = FALSE){
		$this->view_modes = $view_modes;
		$custom_fields = array();
		$all_view_modes = ctools_export_crud_load_all('ds_view_modes');
		$all_view_modes['full'] = (object) array(
			'view_mode' => 'full',
			'label' => 'Full',
		);
		$all_view_modes['default'] = (object) array(
			'view_mode' => 'default',
			'label' => 'Default',
		);
		foreach($view_modes as $name => $val){
			$this->view_modes[$name] = array();
			foreach($bundles as $delta => $bundle){
				$this->view_modes[$name][$bundle] = array();
				$this->view_modes[$name][$bundle]['layout_settings'] = ds_get_layout($entity_type, $bundle, $name);
				$view_mode_settings = !empty($all_view_modes[$name]) ? $all_view_modes[$name] : '';
				if(!empty($this->view_modes[$name][$bundle]['layout_settings'])){
					if(!empty($view_mode_settings)){
						$export->advanced_ds['view_modes'][$name] = $view_mode_settings;
					}
					$field_settings = ds_get_field_settings($entity_type, $bundle, $name);
					if(!empty($field_settings)){
						$this->view_modes[$name][$bundle]['field_settings'] = $field_settings;
					}
					$instances = field_info_instances($entity_type, $bundle);
					if(!empty($instances)){
						$this->view_modes[$name][$bundle]['field_instances'] = $this->extractInstanceViewModeSettings($instances, $name);
					}
					if($extract_custom_fields){
						$custom_fields[$name][$bundle] = $bundle;
					}
					if($field_groups){
						$entity_field_groups = field_group_info_groups($entity_type, $bundle, $name);
						if(!empty($entity_field_groups)){
							$this->view_modes[$name][$bundle]['field_groups'] = $entity_field_groups;
						}
					}
				}else{
					unset($this->view_modes[$name][$bundle]);
				}
			}
		}
		if($extract_custom_fields){
			bt_export_ds_extract_multiple_bundle_custom_fields($custom_fields, $export);
		}
		$export->advanced_ds[$entity_type] = $this->view_modes;
	}


	/**
	 * Extracts the desired view modes from the entity types being exported
   */
	public function extractInstanceViewModeSettings(array $instances, $view_mode){
		$view_mode_instances = array();
		if(!empty($instances)){
			foreach($instances as $field_name => $instance_values){
				if(!empty($instance_values['display'][$view_mode])){
					$view_mode_instances[$field_name] = array();
					$view_mode_instances[$field_name][$view_mode] = $instance_values['display'][$view_mode];
				} else if(!empty($instance_values['display']['default'])){
						$view_mode_instances[$field_name] = array();
						$view_mode_instances[$field_name]['default'] = $instance_values['display']['default'];
					}
			}
			return $view_mode_instances;
		}
	}

}