<?php

/**
 * @file bt_export_ajax_import.class.php
 * Class for handling the data import via ajax
 */


/**
 * @class BtAjaxImport
 * Prepares and extracts the required data from the $form_state
 * and passes it to the main import class bt_import_data.class.php
 * return formatted code to be imported
 */
class BtAjaxImport{

	public $trigger = array();
	public $import_data = array();
	public $import_code = array();
	public $function_data = array();
	public $function_cache = array();
	public $results = array();
	public $target;
	public $entity_type = 'bundle';
	public $form_state;


	/**
	 * sets the properties needed for the function in this class
	 * @param $form_state
	 * the $form_state from our import form when an importing ajax button is triggered
	 */
	public function __construct(&$form_state){
		if(!empty($form_state['triggering_element']['#name'])){
			$this->trigger = explode('|', $form_state['triggering_element']['#name']);
			$this->import_data = &$form_state['import_data'];
			$this->function_data = $form_state['import_function_data'];
			$this->form_state = &$form_state;
		}
	}
	
	/**
   * Loops through the trigger property to find the values needed from form_state
	 */
	public function findImportCode(){
		$trigger = $this->trigger;
		array_walk($trigger, array($this, 'extractImportCode'));
		if(!empty($this->import_code)){
			$this->findFunctionMethod();
		}
		$this->findTarget();
	}

	/**
   * $trigger stores keys that we need to extract the needed values from $form_state['import_data'];
   * we loop through them here and store the values we need for import in our $import_code object property
   * @param $key
   *  key from our trigger array
   * @param $component
   *  the component from our trigger array
	 */
	public function extractImportCode($key, $component){
		if(empty($this->import_code)){
			$this->import_code = $this->import_data[$key];
		} else if(!empty($this->import_code[$key])){
				$this->import_code = $this->import_code[$key];
			}
	}

	
	/**
   * Function for entity types being imported containing field_instances
   * Find how many instances the entity has and handles how many field_instances
   * have been imported and need to be imported
	 */
	private function findTarget(){
		$form_state = &$this->form_state;
		$num_of_trigger_elements = count($this->trigger);
		$this->target = $this->trigger[ ($num_of_trigger_elements - 1) ];
		$this->entity_type = $this->trigger[1];
		$data = &$this->import_code;
		$bundle = $this->trigger[0];
		$component = $this->trigger[1];
		switch($bundle){
		case 'node':

			break;
		case 'field_collection_item':

			break;
		case 'bundle':
			$this->entity_type = $this->trigger[0];
			$bundle = $this->trigger[1];
			break;
		}

		switch($this->target) {
		case 'field_instances':
			$instance_import_data = $this->findInstanceImportAmounts($this->entity_type, $bundle, $form_state, $this->target);
			$import_data = $this->extractFieldInstancesByAmount($this->entity_type, $bundle, $this->target, $instance_import_data);
			break ;
		}
	}

	/**
	 * Function to extract a number of field_instances from the entity we are exporting
	 * @param $entity_type
	 *   an entity type
	 * @param $bundle
	 *   The bundle of an entity type
	 * @param $target
	 *   string: the array key storing the values we need ei: field_instances or field_groups
	 */
	public function extractFieldInstancesByAmount($entity_type, $bundle, $target = '', $instance_import_data){
		$import_amount = $instance_import_data['import_amount'];
		$amount_of_instances = $instance_import_data['all'];
		$imported_amount = $instance_import_data['imported'];
		$import_data = &$this->import_data;
		$import_code = &$this->import_code;
		switch($entity_type){
		case 'field_collection_item':
		case 'node':
			$component = $entity_type;
			$entity_type = $bundle;
			$bundle = $component;
			$target = $this->trigger[2];
			$field_instances = $import_data[$entity_type][$bundle][$target]['field_instances'];
			$import_data[$entity_type][$bundle][$target]['field_instances'] = array_slice($import_code, $import_amount, $amount_of_instances, TRUE);
			$import_code = array_slice($import_code, 0, $import_amount, TRUE);
			break;
		case 'bundle':
			$field_instances = $import_data[$entity_type][$bundle]['field_instances'];
			$import_data[$entity_type][$bundle]['field_instances'] = array_slice($import_code, $import_amount, $amount_of_instances, TRUE);
			$import_code = array_slice($import_code, 0, $import_amount, TRUE);
			break;
		}
	}

	/**
   * Finds the field_instance counters in our $form_state based on the amount the user is importing
   * updates the counters here so when the form is rebuilt you can see how many have been imported
   * @param $entity_type
	 *    an entity type
	 * @param $bundle
	 *    The bundle of an entity type
	 * @param $form_state
	 * @param $target
	 *    
	 */
	private function findInstanceImportAmounts($entity_type, $bundle, &$form_state, $target = ''){

		switch($entity_type){
		case 'node':
		case 'field_collection_item':
			$target = $this->trigger[2];
			$input_textfield_name = t('@entity_type|@bundle|@view_mode|field_instances_amount', array('@bundle' => $bundle, '@entity_type' => $entity_type, '@view_mode' => $target));
			$all_instances_amount = $form_state['import_component_counters'][$entity_type][$bundle][$target]['field_instances']['all'];
			$instance_imported_amount = &$form_state['import_component_counters'][$entity_type][$bundle][$target]['field_instances']['imported'];
			break;
		case 'bundle':
			$input_textfield_name = t('@entity_type|@bundle|field_instances_amount', array('@bundle' => $bundle, '@entity_type' => $entity_type));
			$all_instances_amount = $form_state['import_component_counters'][$entity_type][$bundle]['field_instances']['all'];
			$instance_imported_amount = &$form_state['import_component_counters'][$entity_type][$bundle]['field_instances']['imported'];
			break;
		}
		$instance_import_amount = $form_state['values'][$input_textfield_name];

		if(empty($instance_import_amount)){
			$instance_import_amount = $all_instances_amount;
		}
		if(empty($instance_imported_amount)){
			$instance_imported_amount = ($instance_import_amount);
		} else {
			$instance_imported_amount = ($instance_imported_amount + $instance_import_amount);
		}

		if($instance_imported_amount > $all_instances_amount){
			$instance_imported_amount = $all_instances_amount;
		}

		return array('import_amount' => $instance_import_amount, 'all' => $all_instances_amount, 'imported' => $instance_imported_amount);
	}

  /**
   * Loops through the function data to find the function we need to call to import this entity
	 */
	private function findFunctionMethod(){
		$trigger = $this->trigger;
		$function_data = $this->function_data;
		array_walk_recursive($trigger, array($this, 'eliminateArrayElements'));
	}

	/**
   * Function to find function to be run in our BtImportManager class.
   * each ajax field stores these function in $form_state['import_function_data'] 
   * @see class BtImportManager
	 */
	private function eliminateArrayElements($key, $import_component){
		$function_data = $this->function_data;
		$function_cache = &$this->function_cache;
		if(empty($function_cache)){
			$function_cache = $function_data[$key];
		} else if(!empty($function_cache[$key])){
				$function_cache = $function_cache[$key];
			}
	}
	
	/**
   * Function that takes all the modified $import_code and function to run and runs them.
   * @see class BtImportManager
   * this is where all the importing starts to take place
   * @return $results
   *   the results returned from BtImportManager->cleeanUp
	 */
	public function runImportFunction(){
		$results = &$this->results;
		if(!empty($this->import_code) && !empty($this->function_cache)){
			if(!empty($this->function_cache['construct_args'])){
				$function_arguments = implode(', ', $this->function_cache['construct_args']);
				if(!empty($this->import_code)){
					$import = new BtImportManager($this->import_code, $function_arguments);
					if(!empty($this->function_cache['functions'])){
						foreach($this->function_cache['functions'] as $function_name => $function_args){
							$import->addFunctionArguments($function_args);
							$import->$function_name();
							$results = $import->cleanUp();
						}
					}
				}
			}
		}
		return $results;
	}
}