<?php

/**
 * @file bt_import_data.class.php
 * Main class for import data
 */


//contains most function for importing
class BtImportManager {

	public $component;
	public $import_data = array();
	const MESSAGE_UPTODATE = 'messages warning';
	const MESSAGE_NEW = 'messages status';
	const MESSAGE_ERROR = 'message error';
	public $messages = array();
	public $function_args = array();
	public $bundle;


	/**
	 * constructs the import component (entity_type or bundle) and import code.
	 * @param $data
	 *   array: the import data
	 * @param $component
	 *   string: entity_type or bundle
	 */
	public function __construct($data = array(), $component = ''){
		$this->component = $component;
		$this->import_data = $data;
	}

	/**
	 * function to set function arguments needed for later function calls
	 * @see class BtAjaxImport function runImportFunction
	 */
	public function addFunctionArguments($arguments = array()){
		$this->function_args = $arguments;
	}

	/**
	 * returns the results from importing
	 */
	public function cleanUp(){
		return $this->messages;
	}

	/**
	 * Function to find the entity type and desirect import method
	 * runs the import_code through the function needed to import
	 * @param $target
	 *   A key to the values in our import_data array
	 * @param $entity_type
	 *   an entity type
	 * @param $bundle
	 *   a bundle
	 */
	public function importTarget($target = '', $entity_type = 'node', $bundle = ''){
		if(empty($target) && empty($bundle) && !empty($this->function_args)){
			$target = $this->function_args[0];
			$entity_type = $this->function_args[1];
			$bundle = $this->function_args[2];
		}
		$data = $this->import_data;
		switch($this->component){
		case 'bundle':
			switch($target){
			case 'field_instances':
				$this->importUpdateFieldInstances($data, $entity_type, $bundle, TRUE);
				break;
			case 'settings':
				$this->importBundle($data);
				break;
			case 'fields':
				$this->importCreateFields($data['fields']);
				break;
			case 'field_groups':
				$this->importBundleFieldGroups($data);
				break;
			case 'ds':
				$this->importBundleDsSettings($data, $bundle);
				break;
			case 'field_collections':
				$this->importFieldCollections($data, $bundle);
				break;
			}
			break;
		case 'view_mode':
			$this->importEntityViewMode($data);
			break;
		}
	}

	/**
	 * Function to import field collection properties
	 * @param $bundle
	 *   a node field_collection bundle
	 * @param $collections
	 *   array: an array of field_collection bundles and properties.
	 */
	public function importFieldCollections($collections, $bundle = ''){
		foreach($collections as $bundle => $components){
			foreach($components as $type => $values){
				switch($type){
				case 'ds':
					$this->importBundleDsSettings($values, $bundle, 'field_collection_item');
					break;
				case 'field_instances':
					$this->importUpdateFieldInstances($values, 'field_collection_item', $bundle, TRUE);
					break;
				case 'field_settings':
					$values = array($bundle => $values);
					$this->importCreateFields($values, 'field_colletion_item');
					break;
				case 'field_groups':
					foreach($values as $view_mode => $groups){
						if(!empty($groups)){
							foreach($groups as $field_group_name => $field_group_values){
								$this->fieldGroupExists($field_group_name, 'field_collection_item', $bundle, $view_mode, $field_group_values);
							}
						}
					}
					break;
				case 'bundle':

					break;
				}
			}
		}
	}

	/**
	 * Function to create a bundle
	 * @param $settings
	 *   object: bundle settings
	 */
	public function importBundle($settings){
		if($exists = node_type_load($settings->type)){
			$message = t('Bundle @bundle up to date', array('@bundle' => $settings->type));
			$this->importMessage($message, self::MESSAGE_UPTODATE);
		}else{
			node_type_save($settings);
			$message = t('Created Bundle @bundle', array('@bundle' => $settings->type));
			$this->importMessage($message, self::MESSAGE_NEW);
		}
	}

	/**
	 * Function to import Display Suite view modes and properties
	 * @param $view_mode
	 *   string: a view mode machine name being imported
	 * @param $entity_type
	 *   an entity type
	 * @param $target
	 *   the property name to the data being imported
	 * @param $bundle
	 *   string: bundle name
	 *
	 */
	public function importAdvancedDs($view_mode = '', $entity_type = 'node', $target = '', $bundle = ''){
		if(empty($view_mode) && empty($target) && !empty($this->function_args)){
			$view_mode = $this->function_args[0];
			$entity_type = $this->function_args[1];
			$target = $this->function_args[2];
		}
		$data = $this->import_data;
		switch($entity_type){
		case 'field_collection_item':
		case 'node':
			switch($target){
			case 'field_instances':
				$this->importUpdateFieldInstances($data, $entity_type, $bundle);
				break;
			case 'field_settings':
				$table = 'ds_field_settings';
				$this->importFieldSettings($data, $entity_type, $view_mode, $table);
				break;
			case 'layout_settings':
				$table = 'ds_layout_settings';
				$this->importFieldSettings($data, $entity_type, $view_mode, $table, TRUE);
				break;
			case 'field_groups':
				$this->importFieldGroups($entity_type, $view_mode, $data);
				break;
			}
			break;
		}
	}

	/**
	 * Function to import Display Suite and Form field groups for node bundles
	 * @param $field_groups
	 *   array: an array of field groups belonging to specific entity_type and bundle
	 * @param $bundle
	 *   string: a bundle
	 * @param $view_mode
	 *   string: view mode of the field groups being imported
	 */
	public function importBundleFieldGroups($field_groups, $entity_type = 'node', $bundle = '', $view_mode = 'form'){
		foreach($field_groups as $type => $values){
			switch($type){
			case 'form':
				foreach($values as $field_group_name => $field_group_values){
					$bundle = $field_group_values->bundle;
					$entity_type = $field_group_values->entity_type;
					$this->fieldGroupExists($field_group_name, $entity_type, $bundle, 'form', $field_group_values);
				}
				break;
			case 'ds':
				foreach($values as $view_mode => $groups){
					foreach($groups as $field_group_name => $field_group_values){
						$bundle = $field_group_values->bundle;
						$entity_type = $field_group_values->entity_type;
						$this->fieldGroupExists($field_group_name, $entity_type, $bundle, $view_mode, $field_group_values);
					}
				}
				break;
			}
		}
	}

	/**
	 * Function to check if the field group exists
	 * If it doesn't we create the new field group
	 * otherwise we check to see if it needs updated.
	 * @param $field_group_name
	 *   string: the name of the field group
	 * @param $entity_type
	 *   string: the field groups entity type
	 * @param $bundle
	 *   string: bundle of the field group being imported
	 * @param $view_mode
	 *   string: the view mode of the field group being imported
	 * @param $field_group_values
	 *   object: a field group object
	 */
	private function fieldGroupExists($field_group_name, $entity_type, $bundle, $view_mode, $field_group_values){
		if(!$current_field_group = field_group_exists($field_group_name, $entity_type, $bundle, $view_mode)){
			$field_group_values->export_type = NULL;
			unset($field_group_values->id);
			//save the field groups
			$import_field_group = field_group_group_save($field_group_values);
			if($import_field_group){
				$message = t('Created Field Group @group', array('@group' => $field_group_name));
				$this->importMessage($message, self::MESSAGE_NEW);
			} else {
				$message = t('Failed to create Field Group @group', array('@group' => $field_group_name));
				$this->importMessage($message, self::MESSAGE_ERROR);
			}
		} else if(!$field_group_values === $current_field_group){
				// @TODO Update field_group if values have changed
				/*
module_invoke_all('field_group_update_field_group', $field_group_values);
				$message = t('Updated Field Group @group', array('@group' => $field_group_name));
				$this->importMessage($message, self::MESSAGE_NEW);
*/

			} else {
			$message = t('Field Group @group up to date', array('@group' => $field_group_name));
			$this->importMessage($message, self::MESSAGE_UPTODATE);
		}
	}

	/**
	 * Function to import field groups
	 * @param $entity_type
	 *   string: the entity_type of the field group
	 * @param $view_mode
	 *   string: the view mode for the field group
	 * @param $field_groups
	 *   array: an array of field groups belonging to the variable above
	 */
	public function importFieldGroups($entity_type, $view_mode, $field_groups = array()){
		foreach($field_groups as $bundle => $field_group){
			foreach($field_group as $field_group_name => $field_group_values){
				$this->fieldGroupExists($field_group_name, $entity_type, $bundle, $view_mode, $field_group_values);
			}
		}
	}

	/**
	 * Function for importing Display Suite layout_settings and field_settings
	 * @param $bundles
	 *    array: an array of bundles containing layout_settings and field_setting values
	 * @param $entity_type
	 *    the entity type for the data being imported
	 * @param $view_mode
	 *    string: the view mode the settings belong to
	 * @param $table
	 *    the MYSQL table needed to import the layout and field settings
	 * @param $update_view_mode
	 *    bool: whether not to update the view mode instances
	 */
	private function importFieldSettings(array $bundles, $entity_type, $view_mode = '', $table, $update_view_mode = FALSE){
		foreach($bundles as $bundle => $fields){
			$this->queryDsTableValues($fields, $bundle, $view_mode, $table, $entity_type, $update_view_mode);
		}
	}

	/**
	 * Function to import node bundle Display Suite layout and field settings
	 * @param $ds
	 *   array: an array of view modes containing layout and field settings
	 * @param $bundle
	 *   string: the bundle for settings being imported
	 * @param $entity_type
	 *   string: the entity type for the settings being imported
	 */
	private function importBundleDsSettings($ds, $bundle, $entity_type = 'node'){
		foreach($ds as $view_mode => $values){
			foreach($values as $component => $comp_values){
				switch($component){
				case 'ds_layout_settings':
					$this->queryDsTableValues($comp_values, $bundle, $view_mode, $component, $entity_type, TRUE);
					break;
				case 'ds_field_settings':
					$this->queryDsTableValues($comp_values, $bundle, $view_mode, $component, $entity_type, TRUE);
					break;
				case 'view_mode_properties':

					break;
				}
			}
		}
	}

	/**
	 * Handles all the Display Suite importing
	 * querys to see if the settings exists and need to be updated
	 * creates or updates Display Suite layout_setting and field_settings in the database
	 * @param $ds_values
	 *   array: A Bundles Display Suite layout_settings or field_settings
	 * @param $bundle
	 *   string: the bundle for settings being imported
	 * @param $view_mode
	 *   string: the view mode for the settings being imported
	 * @param $table
	 *   string: the table that needs to be updated
	 * @param $entity_type
	 *   string: the entity type for the settings being imported
	 */
	private function queryDsTableValues(array $ds_values, $bundle = '', $view_mode = '', $table, $entity_type, $update_view_mode = FALSE){
		if($update_view_mode){
			$this->importUpdateEntityViewMode($entity_type, $bundle, $view_mode);
		}
		$id = $entity_type . '|' . $bundle . '|' . $view_mode;
		$ds_settings = array(
			'id' => $id,
			'entity_type' => $entity_type,
			'bundle' => $bundle,
			'view_mode' => $view_mode,
			'settings' => array(),
		);
		switch($table){
		case 'ds_field_settings':
			$ds_settings['settings'] = $ds_values;
			break;
		case 'ds_layout_settings':
			$layout = $ds_values['layout'];
			$ds_settings['layout'] = $layout;
			$ds_settings['settings'] = !empty($ds_values['settings']) ? $ds_values['settings'] : '';
			break;
		}
		$exists = $this->checkExistingDsSettings($id, $bundle, $table);
		if($exists){
			$this->importUpdateDsSettings($ds_settings, $table, $bundle);
			//@TODO TELL JAVASCRIPT WHAT WE ARE DOING
		}else{
			$this->importDsSettings($ds_settings, $table, $bundle);
			//@TODO TELL JAVASCRIPT WHAT WE ARE DOING
		}
	}

	/**
	 * Updates Display Suite settings in the database
	 * @param $field_values
	 *   array: an array of key values pairs containing the column name and data to be updated
	 * @param $table
	 *   string: the table name to be imported
	 * @param $bundle
	 *   string: the bundle of the settings being updated
	 */
	public function importUpdateDsSettings($field_values, $table, $bundle){
		$field_values['settings'] = serialize($field_values['settings']);
		$update = db_update($table)
		->fields($field_values)
		->condition('id', $field_values['id'], '=')
		->condition('bundle', $field_values['bundle'], '=')
		->execute();
		if($update){
			$message = t('Updated DS table @table for bundle @bundle and View Mode @view_mode', array('@bundle' => $bundle, '@setting' => $field_values['id'], '@table' => $table, '@view_mode' => $field_values['view_mode']));
			$this->importMessage($message, self::MESSAGE_NEW);
		} else {
			$message = t('DS Settings for table @table : @setting and View Mode @view_mode up to date.', array('@table' => $table, '@setting' => $field_values['id'], '@view_mode' => $field_values['view_mode']));
			$this->importMessage($message, self::MESSAGE_UPTODATE);
		}
	}

	/**
	 * returns a render array for each result message
	 * @param $message
	 *   string: a message
	 * @param $status
	 *   string: the status (css class) for drupal_set_message
	 */
	private function importMessage($message, $status){
		$this->messages[] = array(
			'#type' => 'markup',
			'#markup' => $message,
			'#prefix' => '<div class="bt-export-import-result '. $status .'">',
			'#suffix' => '</div>',
		);
	}

	/**
	 * Insert a new row into the database for either Display Suite layout_settings or field_settings
	 * @param $field_values
	 *   array: an array or column name => and values to be updated
	 * @param $table
	 *   string: the table being updated
	 */
	public function importDsSettings($field_values, $table, $bundle){
		$insert = drupal_write_record($table, $field_values);
		if($insert){
			$message = t('Created Settings for @setting and View Mode @view_mode', array('@setting' => $field_values['id'], '@view_mode' => $field_values['view_mode']));
			$this->importMessage($message, self::MESSAGE_NEW);
		}
	}

	/**
	 * queries the db to see if the Display Suite settings exists or need to be updated
	 * @param $id
	 *   string: the column "id" inside ds_layout_settings or ds_field_settings
	 * @param $bundle
	 *   string: the column "bundle" inside ds_layout_settings or ds_field_settings
	 * @param $table
	 *   string: the table to query against
	 */
	public function checkExistingDsSettings($id, $bundle, $table){
		$result = db_select($table, 'ds')
		->fields('ds')
		->condition('id', $id, '=')
		->condition('bundle', $bundle, '=')
		->execute()
		->fetchAssoc();
		return $result;
	}

	/**
	 * Imports a view mode into the database
	 * @param $view_mode
	 *   object: a view mode object
	 */
	public function importEntityViewMode($view_mode){
		if(!empty($view_mode)){
			// Delete previous view_mode configuration (if any)
			db_delete('ds_view_modes')
			->condition('view_mode', $view_mode->view_mode)
			->execute();
			$new_view_mode = new StdClass;
			$new_view_mode->view_mode = $view_mode->view_mode;
			$new_view_mode->label = $view_mode->label;
			$new_view_mode->entities = !empty($view_mode->entities) ? $view_mode->entities : NULL;
			//save the view mode to the database
			$save_view_mode = drupal_write_record('ds_view_modes', $new_view_mode);
			$message = t('Created View Mode @view_mode', array('@view_mode' => $view_mode->view_mode));
			$this->importMessage($message, self::MESSAGE_NEW);
		}
	}

	/**
	 * Updates the view_mode of an entity bundle inside its field_bundle_settings
	 * @param $entity_type
	 *   string: a entity type
	 * @param $bundle
	 *   string: a bundle type
	 * @param $view_mode
	 *   string: the entity bundle view mode
	 */
	public function importUpdateEntityViewMode($entity_type, $bundle, $view_mode){
		//update the bundle settings
		$bundle_settings = field_bundle_settings($entity_type, $bundle);
		if(empty($bundle_settings['view_modes'][$view_mode]['custom_settings'])){
			$bundle_settings['view_modes'][$view_mode]['custom_settings'] = TRUE;
			// Save updated bundle settings.
			$save = field_bundle_settings($entity_type, $bundle, $bundle_settings);
		}
	}

	/**
	 * Imports field_instances for entity types
	 * @param $instances
	 *   array: an array of field_instances
	 * @param entity_type
	 *   string: the entity type
	 * @param $bundle_name
	 *   string: the bundle
	 * @param $create_new_instance
	 *   bool: whether to created a new field instance if one doesn't exist
	 */
	private function importUpdateFieldInstances(array $instances, $entity_type = 'node', $bundle_name = '', $create_new_instance = FALSE){
		if(count($instances > 1) && !empty($bundle_name)){
			$instances = array($bundle_name => $instances);
		}
		foreach($instances as $bundle => $fields){
			foreach($fields as $field_name => $viewmode_settings){
				if($current_instance = field_info_instance($entity_type, $field_name, $bundle)){
					if(!empty($viewmode_settings['display'])){
						$viewmode_settings = $viewmode_settings['display'];
					}
					foreach($viewmode_settings as $view_mode => $settings){
						$change = TRUE;
						if(!empty($current_instance['display'][$view_mode])){
							//if was not a change in the instance display
							if($settings === $current_instance['display'][$view_mode]){
								$change = FALSE;
							}
						}
						if($change){
							$current_instance['display'][$view_mode] = $settings;
							$update_instance = field_update_instance($current_instance);
							$message = t('Updating Field Instance @instance and View Mode @view_mode, for Bundle @bundle', array('@bundle' => $bundle, '@instance' => $field_name, '@view_mode' => $view_mode));
							$this->importMessage($message, self::MESSAGE_NEW);
						} else {
							$message = t('Field Instance @instance and View Mode @view_mode, for Bundle @bundle up to date', array('@bundle' => $bundle, '@instance' => $field_name, '@view_mode' => $view_mode));
							$this->importMessage($message, self::MESSAGE_UPTODATE);
						}
					}
				}else if($create_new_instance == TRUE){
						/*
$additional_params = array(
					 	'include_inactive' => TRUE,
					 	'include_deleted' => TRUE,
					 );
					 $query_field = array(
					 	 'field_name' => $field_name,
					 	 'bundle' => $bundle,
					 );
						$instance_exists = field_read_instances($query_field, $additional_params);
*/
						//@TODO FIX INACTIVE FIELDS
						field_create_instance($viewmode_settings);
						$message = t('Created Field Instance @instance and View Mode @view_mode for bundle @bundle', array('@bundle' => $bundle, '@instance' => $field_name, '@view_mode' => $view_mode));
						$this->importMessage($message, self::MESSAGE_NEW);
					}
			}
		}
	}

	/**
	 * Imports field or bundles
	 * @param $fields
	 *   array: an array with the bundle as key fields as children
	 * @param entity_type
	 *   string: the entity type for the field being imported
	 */
	private function importCreateFields($fields, $entity_type = 'node'){
		$additional_params = array(
			'include_inactive' => TRUE,
			'include_deleted' => TRUE,
		);
		foreach($fields as $bundle => $field){
			foreach($field as $field_name => $field_values){
				$current_field = field_info_field($field_name);
				/*
$query_field = array(
					'field_name' => $field_name,
				);
*/
				$inactive_field = field_read_fields($query_field, $additional_params);
				if(!empty($current_field)){
					//if(empty($inactive_field)){
					$message = t('Field @field for bundle @bundle up to date', array('@field' => $field_name, '@bundle' => $bundle));
					$this->importMessage($message, self::MESSAGE_UPTODATE);
					//}else{
					//@TODO FIX INACTIVE FIELDS
					//}
				}else{
					$create_field = field_create_field($field_values);
					$message = t('Created New Field @field for bundle @bundle', array('@field' => $field_name, '@bundle' => $bundle));
					$this->importMessage($message, self::MESSAGE_NEW);
				}
			}
		}
	}
}