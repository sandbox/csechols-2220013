<?php

/**
 * @file bt_export_form_handler_functions.inc
 * Contains helper functions for the main export / import form.
 */


/**
 * function for return the ajax field when the form is being rebuilt
 */
function _bt_export_build_ajax_field($name, $title){
	return array(
		'#type' => 'submit',
		'#button_type' => 'submit',
		'#value' => $title,
		'#name' => $name,
		'#submit' => array('_bt_export_import_ajax_data'),
		'#ajax' => array(
			'wrapper' => 'bt-export-import-fieldset',
			'callback' => '_bt_export_ajax_form_callback',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#prefix' => '<div class="bt-import-ajax-data">',
		'#suffix' => '</div>',
	);
}

/**
 * function finding the field_instance counter of node bundles
 */
function _bt_export_find_import_bundle_data_counters($values, $form_state, $type){
	$num_of_instances = !empty($form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$type]['all']) ? $form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$type]['all'] : 0;
	$num_of_imported_instances = !empty($form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$type]['imported']) ? $form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$type]['imported'] : 0;
	return array('all' => $num_of_instances, 'imported' => $num_of_imported_instances);
}

/**
 * function finding the field_instance counter of display suite bundles
 */
function _bt_export_find_import_ds_bundle_dta_counters($values, $form_state, $type){
	$num_of_instances = !empty($form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$values['view_mode']][$type]['all']) ? $form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$values['view_mode']][$type]['all'] : 0;
	$num_of_imported_instances = !empty($form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$values['view_mode']][$type]['imported']) ? $form_state['import_component_counters'][$values['entity_type']][$values['bundle']][$values['view_mode']][$type]['imported'] : 0;
	return array('all' => $num_of_instances, 'imported' => $num_of_imported_instances);
}


/**
 * Function for returning the textfield used to enter in the amount of
 * field_instances of an entity
 * @param $max
 *   int: the maximum amount of field_instances of the bundle
 */
function _bt_export_instance_amount_textfield($max){
	return array(
		'#type' => 'textfield',
		'#title' => t('#'),
		'#size' => 3,
		'#prefix' => '<div class="form-item-field-instance-import-amount">',
		'#suffix' => '</div>',
		'#default_value' => $max,
	);
}


/**
 * return a markup form item to display the amount of field_instances imported and left
 */
function _bt_export_instance_amount_markup($instance_amount, $imported_amount){
	return array(
		'#type' => 'markup',
		'#markup' => t('Imported: @imported_instances / @all_instances', array('@imported_instances' => $imported_amount, '@all_instances' => $instance_amount)),
		'#prefix' => '<div class="bt-export-field-instance-import-status">',
		'#suffix' => '</div>',
	);
}

/**
 * return a container to wrap our field_instance ajax field and markup
 */
function _bt_export_field_instance_container(){
	return array(
		'#type' => 'container',
		'#attributes' => array(
			'class' => array('bt-export-ajax-field-instance-container', 'clearfix'),
		),
	);
}